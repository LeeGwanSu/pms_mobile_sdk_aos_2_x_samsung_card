package com.pms.sdk.push;

import android.content.Context;
import android.content.Intent;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.ProPertiesFileUtil;
import com.pms.sdk.push.mqtt.MQTTBinder;
import com.pms.sdk.push.mqtt.MQTTService;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ydmner on 7/27/16.
 */
public class FCMPushService extends FirebaseMessagingService implements IPMSConsts {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            CLog.i("FCM OnMessageReceived From : " + remoteMessage.getFrom());
            CLog.i("Message data payload: " + remoteMessage.getData());

            Intent broadcastIntent = new Intent(getApplicationContext(), PushReceiver.class);
            broadcastIntent.setAction(ACTION_RECEIVE);
//                Intent broadcastIntent = new Intent(ACTION_RECEIVE);
            broadcastIntent.addCategory(getApplication().getPackageName());
            broadcastIntent.putExtra(MQTTBinder.KEY_MSG, new JSONObject(remoteMessage.getData()).toString());
            broadcastIntent.putExtra("message_id", remoteMessage.getMessageId());
            sendBroadcast(broadcastIntent);

        } catch (Exception e) {
            CLog.e(e.getMessage());
        }

        Intent intentPush = null;

        String receiverClass = null;
        receiverClass = ProPertiesFileUtil.getString(getApplicationContext(), PRO_PUSH_RECEIVER_CLASS);
        if (receiverClass != null) {
            try {
                Class<?> cls = Class.forName(receiverClass);
                intentPush = new Intent(getApplicationContext(), cls).putExtras(remoteMessage.toIntent());
                intentPush.setAction(RECEIVER_PUSH);
            } catch (ClassNotFoundException e) {
//                e.printStackTrace();
            }
        }
        if (intentPush == null) {
            intentPush = new Intent(RECEIVER_PUSH).putExtras(remoteMessage.toIntent());
        }

        if (intentPush != null) {
            intentPush.addCategory(getApplicationContext().getPackageName());
            getApplicationContext().sendBroadcast(intentPush);
        }
    }

    @Override
    public void onMessageSent(String msgId) {
        CLog.e("onMessageSent() " + msgId);
        super.onMessageSent(msgId);
    }

    @Override
    public void onSendError(String s, Exception e) {
        CLog.e("onSendError() " + s + ", " + e.toString());
        super.onSendError(s, e);
    }

    @Override
    public void onNewToken(String token)
    {
        super.onNewToken(token);
        //업체 요청으로 인해 해당 값 사용하지 않음
        //FCMRequestToken을 사용하여 다중 FCM 프로젝트에서 사용가능한 토큰 값을 사용함
//        PMSUtil.setGCMToken(getApplicationContext(), token);
//        CLog.i("onNewToken, registration ID = " + token);
    }
}
