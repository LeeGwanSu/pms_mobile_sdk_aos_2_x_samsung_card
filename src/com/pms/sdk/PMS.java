package com.pms.sdk;

import java.io.Serializable;
import java.util.Stack;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.text.TextUtils;

import com.pms.sdk.api.APIManager;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.ProPertiesFileUtil;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.FCMRequestToken;

import okhttp3.OkHttpClient;

/**
 * @since 2012.12.28
 * @author erzisk
 * @description pms (solution main class)
 * @version [2014.06.24 09:10] ReadMSg UserMsgId로 가능하게 수정함. <br>
 *          [2015.02.04 10:23] SDK 사용 중지 요청 가능하게 수정함. <br>
 *          [2015.02.05 19:33] newMsg Update시 DEL_YN값이 null값이 들어가는 현상 수정함. <br>
 *          [2015.02.11 09:55] Flag 값들이 안바뀌는 현상 수정함. <br>
 *          [2015.02.25 11:45] 앱 최초설치시에는 msgFlag & notiFlag 값 서버에서 받아온 값 저장함. <br>
 *          [2015.03.03 16:55] 앱실행시 체크하는 프로세스 롤리팝 버전에 맞게 수정함.<br>
 *          [2015.03.11 10:02] Private 서버 안정화.<br>
 *          [2015.03.17 18:11] 팝업창 노출 플래그 추가함.<br>
 *          [2015.03.19 16:29] DB쪽 버그 수정함.<br>
 *          [2015.03.30 21:34] 팝업 클릭시 ClickMsg & readMsg 직접호출로 수정함.<br>
 *          [2015.04.03 09:40] Notification Image Load가 실패하면 Text Notification으로 전환하는 코스 삽입함.<br>
 *          [2015.04.23 16:59] GCM 받아오는 부분 팝업창 뜨도록 되어 있는부분을 로그로 찍게 수정함.<br>
 *          [2015.06.12 10:21] GCM 미지원시 오류 처리함.<br>
 *          [2015.06.26 10:50] LogoutPms.m 호출시 MaxUserMsgId를 -1로 초기화 하는 루틴 수정함.<br>
 *          [2015.07.20 13:23] Notification Priority추가함.<br>
 *          [2015.07.20 16:43] BigImage시 메세지 내용도 보이게 수정함.<br>
 *          [2015.09.07 10:58] 팝업창 뛰우는 플래그 'N'으로 변경. MSG class pushImg 값 추가함.<br>
 *          [2015.10.02 10:51] Rich일때 pms-sdk.js파일을 로딩안하고 붙도록 수정함.<br>
 *          [2015.10.22 11:08] DeviceCert 시 Sleep 삭제함.<br>
 *          [2015.11.11 14:05] DelMsg를 추가함.<br>
 *          [2016.02.25 17:06] Rich Push 저장시 본래 msggrp값으로 저장하게 수정함.<br>
 *          [2016.03.30 14:25] SSL 관련 내용을 적용함.<br>
 *          [2016.04.05 14:28] 무한 호출 사항 수정함.<br>
 *          [2016.04.12 14:42] 버그 리포트사항 수정함.<br>
 *          [2016.04.26 15:44] 인증키 가져오는 루틴 수정함.<br>
 *          [2016.04.29 10:11] 인증서를 제대로 가져와야지만 붙도록 수정함.<br>
 *          [2016.07.07 09:42] newMsg API에 TargetType을 추가함.<br>
 *          [2016.07.07 16:42] Target Type 기본값 M -> T로 변경.<br>
 *          [2016.07.08 11:31] Notification icon Background 색상 지정하도록 수정함.<br>
 *          [2016.11.10 16:34] Service에서 로그 찍히는 부분 수정함.<br>
 *          [2016.11.11 11:39] Project ID 부분도 안보이게 수정함.<br>
 *          [2016.11.14 11:17] Log Default 부분을 False로 수정함.<br>
 *          [2016.12.02 18:36] doze mode broadcast 패치1 적용 <br>
 *          [2017.04.07 14:56] MQTT Stop NullpoiontException 나는 부분 수정함. <br>
 *          [2017.10.30 15:09] * Queue Manager 추가 Single tone 으로 관리 (Image Loader 및 APIManager)
 *          					* DB 중복 Open으로 인해 강제종료 문제 수정
 *          					* PushPopup 발생 시 강제 종료 문제 수정
 *          					* UUID 생성 로직 변경, Device정보를 이용하여 UUID를 작성하는 경로 이전에 Device 정보 없이 발급하는 방법으로
 *          					  UUID 발급, 발급 실패 시 Device정보를 필요로 하는 UUID 발급 방법으로 발급 시도함
 *          [2018.05.11 01:54] * UUID 생성 로직 원복, UUID 수정될 경우 큰 문제 초래할 수 있음
 *          					Android 8.0 대응, 명시적 호출 설정 및 노티 채널로 Notification 할 수 있도록 수정
 *          [2018.07.20 13:29] Android 8.0 대응 (NotificationChannel, JobScheduler, Intent 제한)
 *          [2018.09.18 11:04] MQTT 간소화 적용, 잠금화면 체크, 이미지캐시, 벨소리 볼륨 대응
 *          [2018.10.24 11:47] MQTT 백그라운드 처리
 *          [2018.10.30 14:45] MQTT 수정
 *          [2018.10.30 15:38] MQTT 수정
 *          [2018.10.30 16:10] MQTT 수정
 *          [2018.10.30 16:37] MQTT 수정
 *          [2018.10.30 17:32] MQTT 수정
 *          [2018.10.30 18:12] MQTT 수정
 *          [2018.10.30 19:14] MQTT 수정
 *          [2018.10.31 10:37] MQTT 수정
 *          [2018.10.31 17:00] MQTT 수정
 *          [2018.11.01 14:45] MQTT 수정
 *          [2018.11.01 15:48] MQTT 수정
 *          [2018.11.05 09:12] MQTT 수정
 *          [2018.11.12 10:57] MSG API 세분화 수정
 *          [2018.11.12 13:45] MSG API 세분화 수정
 *          [2018.11.12 15:09] NotificationComapt으로 수정
 *          [2018.12.05 11:32] FCM 적용, 콜게이트용 커스텀 적용, 다중 프로젝트 등록 기능 추가
 *          [2018.12.12 11:21] MQTTBinder 적용
 *          [2019.01.09 14:43] MQTTStarter 제거, 분기별 서비스 실행 적용, 유지시간 변경
 *          [2019.01.17 15:43] FCM 다중 프로젝트 대응
 *          [2019.01.17 17:43] 삼성카드 요청으로 NotificationCompat 안쓰도록 함
 *          [2019.01.18 09:05] DeviceCert 요청시마다 토큰 갱신하도록 변경
 *          [2019.03.19 15:48] 신규 NewMsgList의 DelYN 값 넣도록 변경, FcmRequestToken 사용빈도 줄임
 *          [2019.04.01 13:40] NewMsgContent에서 세부 데이터 값 저장 로직 변경
 *          [2019.07.11 14:17] 벨소리 조절 로직 제거
 *          [2019.10.02 17:42] Volley->OkHttp 변경, ReadMsg kk->HH 적용, DeviceCert AsyncTask 제거, Czip close 대응, TlsSocketFactory 적용, DB close 대응, 채널 재생성 로직 제거, MQTTBinder 대응
 *          [2019.11.07 14:06] Android 10 IMEI 값 못가져오는 이슈 대응하기 위해 DeviceCert에 AndroidId 파라미터 추가함
 *          [2019.11.12 14:59] DeviceCert API 호출 후 UUID 업데이트 제거하는 로직 제거
 *          [2020.01.14 09:38] 삼성카드 요청에 따른 알림 적재화, 그룹화, 뱃지 지원 + OkHTTP 최적화 (홈앤쇼핑에 했던거 그대로)
 *          [2020.02.05 10:15] 알림 메시지 갯수 가져올때 integer 파싱 안될 경우 처리
 *          [2020.03.18 14:27] updateNotificationSummary 버전 체크 추가
 *          [2020.03.19 14:55] 업체 요청에 따른 Android X 대응
 *          [2020.03.27 14:40] 뱃지 1로 뜨는 문제 수정
 *			[2020.03.27 15:22] 뱃지 1로 뜨는 문제 수정 (삼성 단말)
 *			[2020.03.27 15:32] 뱃지 1로 뜨는 문제 수정 (픽셀 단말)
 *			[2020.04.08 15:51] 삼성 베이비스토리에서 포그라운드 푸시를 사용하는데 NotificationSummary가 0개의 메시지가 표시되는 현상 대응
 *			[2020.05.26 14:13] 푸시 알림 수신시 최근 메시지 대신 이전 메시지가 우선적으로 표시되는 현상 대응
 *			[2020.09.03 10:25] 요약 알림이 남는 오류 수정
 *			[2020.09.25 14:52] 그룹화 적용시 알림 2개 보유중 하나 클릭시 알림 표시되는 현상 수정
 *			*/
public class PMS implements IPMSConsts, Serializable {

	private static final long serialVersionUID = 1L;

	private static PMS instancePms = null;

	private static PMSPopup instancePmsPopup = null;
	private Context mContext = null;

	private PMSDB mDB = null;

	private static boolean mIsBack = true;

	private PMS(Context context) {
		this.mDB = PMSDB.getInstance(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:"+PMS_VERSION_UPDATE_DATE);

		initOption(context);
	}

	public static PMS getInstance (Context context) {
		CLog.setDebugMode(context);
		CLog.setDebugName(context);

		CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
		if (instancePms == null) {
			instancePms = new PMS(context);
		}
		instancePms.setmContext(context);

		if (StringUtil.isEmpty(PMSUtil.getGCMToken(context)) || NO_TOKEN.equals(PMSUtil.getGCMToken(context)))
		{
			new FCMRequestToken(context, ProPertiesFileUtil.getString(context, IPMSConsts.PRO_GCM_PROJECT_ID), new FCMRequestToken.Callback() {
				@Override
				public void callback(boolean isSuccess, String message) {
					CLog.i("FCMRequestToken() isSuccess ? "+isSuccess+" message "+message);
				}
			}).execute();
		}
		return instancePms;
	}

	public static PMSPopup getPopUpInstance () {
		return instancePmsPopup;
	}

	public static boolean clear () {
		try {
			PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
			instancePms = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void initOption (Context context) {
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_RING_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_RING_FLAG, "Y");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_VIBE_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_VIBE_FLAG, "Y");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_ALERT_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_ALERT_FLAG, "N");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_MAX_USER_MSG_ID))) {
			DataKeyUtil.setDBKey(context, DB_MAX_USER_MSG_ID, "-1");
		}
	}

	public void setCustId (String custId) {
		PMSUtil.setCustId(mContext, custId);
	}

	public String getCustId () {
		return PMSUtil.getCustId(mContext);
	}

	private void setmContext (Context context) {
		this.mContext = context;
	}

	public void setPopupSetting (Boolean state, String title) {
		instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
	}

	public void setIsPopupActivity (Boolean ispopup) {
		PMSUtil.setPopupActivity(mContext, ispopup);
	}

	public void setNotiOrPopup (Boolean isnotiorpopup) {
		PMSUtil.setNotiOrPopup(mContext, isnotiorpopup);
	}

	public void setRingMode (boolean isRingMode) {
		DataKeyUtil.setDBKey(mContext, DB_RING_FLAG, isRingMode ? "Y" : "N");
	}

	public void setVibeMode (boolean isVibeMode) {
		DataKeyUtil.setDBKey(mContext, DB_VIBE_FLAG, isVibeMode ? "Y" : "N");
	}

	public void setPopupNoti (boolean isShowPopup) {
		DataKeyUtil.setDBKey(mContext, DB_ALERT_FLAG, isShowPopup ? "Y" : "N");
	}

	public void setAppBackMode (boolean isBack) {
		CLog.i("isBack set before => " + isBack);
		mIsBack = isBack;
		CLog.i("isBack set after => " + mIsBack);
	}

	public boolean getAppBackMode () {
		CLog.i("isBack get => " + mIsBack);
		return mIsBack;
	}

	public String getMsgFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_MSG_FLAG);
	}

	public String getNotiFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_NOTI_FLAG);
	}

	public String getMaxUserMsgId ()
	{

//		return DataKeyUtil.getDBKey(mContext, DB_MAX_USER_MSG_ID);
		return mDB.selectLastUserMsgId();
	}

	public int getUnReadCount() {
		try
		{
			int count = Integer.valueOf(DataKeyUtil.getDBKey(mContext, DB_NEW_MSG_COUNT));
			return count;
		}catch (Exception e)
		{
			return 0;
		}
//		return mDB.getAllOfUnreadMsgCount();
	}

//	public String getMsgContent() {
//		return DataKeyUtil.getDBKey(mContext, DB_NEW_MSG_CONTENT);
//	}

	/*
	 * ===================================================== [start] database =====================================================
	 */
	/**
	 * select MsgGrp list
	 * 
	 * @return
	 */
	public Cursor selectMsgGrpList () {
		return mDB.selectMsgGrpList();
	}

	/**
	 * select MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public MsgGrp selectMsGrp (String msgCode) {
		return mDB.selectMsgGrp(msgCode);
	}

	/**
	 * select new msg Cnt
	 * 
	 * @return
	 */
	public int selectNewMsgCnt () {
		return mDB.selectNewMsgCnt();
	}

	/**
	 * select Msg List
	 * 
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row) {
		return mDB.selectMsgList(page, row);
	}

	/**
	 * select Msg List
	 * 
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgList (String msgCode) {
		return mDB.selectMsgList(msgCode);
	}

	/**
	 * select msg list
	 * 
	 * @param msgCode
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (String msgCode, int page, int row) {
		return mDB.selectMsgList(msgCode, page, row);
	}

	/**
	 * select target type list
	 * 
	 * @param targetType
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectTargetTypeMsgList (String targetType, int page, int row) {
		return mDB.selectTargetTypeMsgList(targetType, page, row);
	}

	/**
	 * select target type list
	 * 
	 * @param targetType
	 * @param msgCode
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectTargetTypeMsgList (String targetType, String msgCode, int page, int row) {
		return mDB.selectTargetTypeMsgList(targetType, msgCode, page, row);
	}

	/**
	 * select Msg
	 * 
	 * @param msgId
	 * @return
	 */
	public Msg selectMsgWhereMsgId (String msgId) {
		return mDB.selectMsgWhereMsgId(msgId);
	}

	/**
	 * select Msg
	 * 
	 * @param userMsgID
	 * @return
	 */
	public Msg selectMsgWhereUserMsgId (String userMsgID) {
		return mDB.selectMsgWhereUserMsgId(userMsgID);
	}

	/**
	 * select msg(content)
	 * @param userMsgID
	 * @return
	 */
	public boolean selectContentWhereUserMsgId (String userMsgID)
	{
		Msg msg = mDB.selectMsgWhereUserMsgId(userMsgID);
		return !TextUtils.isEmpty(msg.msgText);
	}

	/**
	 * update MsgGrp
	 * 
	 * @param msgCode
	 * @param values
	 * @return
	 */
	public long updateMsgGrp (String msgCode, ContentValues values) {
		return mDB.updateMsgGrp(msgCode, values);
	}

	/**
	 * update read msg
	 * 
	 * @param msgGrpCd
	 * @param firstUserMsgId
	 * @param lastUserMsgId
	 * @return
	 */
	public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
	}

	/**
	 * update read msg where userMsgId
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId (String userMsgId) {
		return mDB.updateReadMsgWhereUserMsgId(userMsgId);
	}

	/**
	 * update read msg where msgId
	 * 
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId (String msgId) {
		return mDB.updateReadMsgWhereMsgId(msgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long deleteUserMsgId (String userMsgId) {
		return mDB.deleteUserMsgId(userMsgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param MsgId
	 * @return
	 */
	public long deleteMsgId (String MsgId) {
		return mDB.deleteMsgId(MsgId);
	}

	/**
	 * delete MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp (String msgCode) {
		return mDB.deleteMsgGrp(msgCode);
	}

	/**
	 * delete expire Msg
	 * 
	 * @return
	 */
	public long deleteExpireMsg () {
		return mDB.deleteExpireMsg();
	}

	/**
	 * delete empty MsgGrp
	 * 
	 * @return
	 */
	public void deleteEmptyMsgGrp () {
		mDB.deleteEmptyMsgGrp();
	}

	/**
	 * delete all
	 */
	public void deleteAll () {
		mDB.deleteAll();
	}

	/*
	 * ===================================================== [end] database =====================================================
	 */

	public OkHttpClient getOkHttpClient(Context context)
	{
		return APIManager.getInstance(context).getOkHttpClient();
	}
	public void setNotificationStackable(boolean isEnable)
	{
		DataKeyUtil.setDBKey(mContext, DB_NOTIFICATION_STACKABLE, isEnable?FLAG_Y:FLAG_N);
	}
	public void setNotificationGroupable(boolean isEnable)
	{
		DataKeyUtil.setDBKey(mContext, DB_NOTIFICATION_GROUPABLE, isEnable?FLAG_Y:FLAG_N);
	}
	public void setNotificationBadgeEnable(boolean isEnable)
	{
		DataKeyUtil.setDBKey(mContext, DB_NOTIFICATION_BADGE_ENABLE, isEnable?FLAG_Y:FLAG_N);
	}

	public int getAllOfUnreadCountFromServer() {
		try
		{
			int count = Integer.valueOf(DataKeyUtil.getDBKey(mContext, DB_NEW_MSG_COUNT));
			return count;
		}catch (Exception e)
		{
			return 0;
		}
//		return mDB.getAllOfUnreadMsgCount();
	}

	public int getAllOfUnreadCount() {return mDB.getAllOfUnreadMsgCount();}

	public int getUnreadCountByMsgGrpCode(String msgGrpCode) {return mDB.getUnreadMsgCountByMsgGrpCode(msgGrpCode);}
	public Cursor selectAllOfMsg () {
		return mDB.selectAllOfMsg();
	}
	public Cursor selectAllOfMsgByMsgGrpCode (String msgGrpCode) {
		return mDB.selectAllOfMsgByMsgGrpCode(msgGrpCode);
	}
	public Cursor selectAllOfMsgGrp () {
		return mDB.selectAllOfMsgGrp();
	}
}
