package com.pms.sdk.bean;

import android.os.Bundle;

import com.pms.sdk.IPMSConsts;

import org.json.JSONException;
import org.json.JSONObject;

public class PushMsg implements IPMSConsts {

	public String msgId;
	public String notiTitle;
	public String notiMsg;
	public String notiImg;
	public String message;
	public String sound;
	public String msgType;
	public String data;
	public int notificationId;

	public PushMsg(Bundle extras) {
		msgId = extras.getString(KEY_MSG_ID);
		notiTitle = extras.getString(KEY_NOTI_TITLE);
		notiMsg = extras.getString(KEY_NOTI_MSG);
		notiImg = extras.getString(KEY_NOTI_IMG);
		message = extras.getString(KEY_MSG);
		sound = extras.getString(KEY_SOUND);
		msgType = extras.getString(KEY_MSG_TYPE);
		data = extras.getString(KEY_DATA);
		notificationId = extras.getInt(KEY_NOTIFICATION_ID);
	}

	@Override
	public String toString () {
		return String.format("onMessage:msgId=%s, notiTitle=%s, notiMsg=%s, notiImg=%s, message=%s, sound=%s, msgType=%s, data=%s, notificationId=%d", msgId, notiTitle,
				notiMsg, notiImg, message, sound, msgType, data,notificationId);
	}

	public String getMsgId()
	{
		return msgId;
	}

	public String getAppLink()
	{
		String appLink = "";
		try {
			JSONObject jsonObject = new JSONObject(data);
			if (jsonObject.has("l"))
			{
				appLink = jsonObject.getString("l");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return appLink;
	}

	public String getNotiTitle()
	{
		return notiTitle;
	}

	public String getNotiMsg()
	{
		return notiMsg;
	}

	public String getNotiImg()
	{
		return notiImg;
	}

	public String getMessage()
	{
		return message;
	}

	public String getSound()
	{
		return sound;
	}

	public String getMsgType()
	{
		return msgType;
	}

	public String getData()
	{
		return data;
	}

	public int getNotificationId()
	{
		return notificationId;
	}
}
