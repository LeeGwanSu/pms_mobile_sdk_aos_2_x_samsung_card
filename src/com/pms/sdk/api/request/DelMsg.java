package com.pms.sdk.api.request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.db.PMSDB;

public class DelMsg implements IPMSConsts
{

	private Context mContext;
	private APIManager apiManager;
	private Prefs mPrefs;
	private PMSDB mDB;

	public DelMsg(Context context) {
		this.mContext = context;
		this.apiManager = APIManager.getInstance(context);
		this.mPrefs = apiManager.getPrefs();
		this.mDB = apiManager.getPmsDb();
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (JSONArray userMsgIds) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("userMsgIds", userMsgIds);

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final JSONArray userMsgIds, final APICallback apiCallback) {
		try {
			apiManager.call(API_DEL_MSG_PMS, getParam(userMsgIds), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json, userMsgIds);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json, JSONArray userMsgIds) {
		try {
			for (int i = 0; i < userMsgIds.length(); i++) {
				mDB.deleteUserMsgId(userMsgIds.getString(i));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return true;
	}

}
