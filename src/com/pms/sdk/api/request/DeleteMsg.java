package com.pms.sdk.api.request;

import android.content.Context;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;

import org.json.JSONException;
import org.json.JSONObject;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.db.PMSDB;

public class DeleteMsg implements IPMSConsts
{

    private Context mContext;
    private APIManager apiManager;
    private Prefs mPrefs;
    private PMSDB mDB;

    public DeleteMsg(Context context) {
        this.mContext = context;
        this.apiManager = APIManager.getInstance(context);
        this.mPrefs = apiManager.getPrefs();
        this.mDB = apiManager.getPmsDb();
    }

    public JSONObject getParam (String msgId) {
        JSONObject jobj;

        try {
            jobj = new JSONObject();
            jobj.put("msgId", msgId);

            return jobj;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void request (String msgId, final APICallback apiCallback) {
        try {
            apiManager.call(API_DELETE_MSG, getParam(msgId), new APICallback() {
                @Override
                public void response (String code, JSONObject json) {
                    if (CODE_SUCCESS.equals(code)) {
                        requiredResultProc(json);
                    }
                    if (apiCallback != null) {
                        apiCallback.response(code, json);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean requiredResultProc (JSONObject json) {
        return true;
    }
}
