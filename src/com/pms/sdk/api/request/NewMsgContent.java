package com.pms.sdk.api.request;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.db.PMSDB;

public class NewMsgContent implements IPMSConsts
{
    private Context mContext;
    private APIManager apiManager;
    private Prefs mPrefs;
    private PMSDB mDB;

    public NewMsgContent(Context context) {
        this.mContext = context;
        this.apiManager = APIManager.getInstance(context);
        this.mPrefs = apiManager.getPrefs();
        this.mDB = apiManager.getPmsDb();
    }

    public JSONObject getParam (String reqUserMsgId) {
        JSONObject jobj;

        try {
            jobj = new JSONObject();
            PMS pms = PMS.getInstance(mContext);
            jobj.put("reqUserMsgId", reqUserMsgId);

            return jobj;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void request (final String reqUserMsgId, final APICallback apiCallback) {
        try {
            apiManager.call(API_NEW_MSG_CONTENT, getParam(reqUserMsgId), new APICallback() {
                @Override
                public void response (String code, JSONObject json) {
                    if (CODE_SUCCESS.equals(code)) {
                        requiredResultProc(json, reqUserMsgId);
                    }
                    if (apiCallback != null) {
                        apiCallback.response(code, json);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean requiredResultProc (JSONObject json, String reqUserMsgId) {
        try {
            if (json.getString("code").equals("000")) {
                if (json.has("msgContent")) {
                    JSONObject obj = json.getJSONObject("msgContent");

                    Msg msg = mDB.selectMsgWhereUserMsgId(reqUserMsgId);
                    if (msg == null)
                    {
                        CLog.d("newMsg is not found.");
                        return false;
                    }
                    msg.appLink = obj.getString("appLink");
                    msg.pushMsg = obj.getString("pushMsg");
                    msg.pushImg = obj.getString("pushImg");
                    msg.msgText = obj.getString("msgText");
                    msg.msgType = obj.getString("msgType");
//                    msg.msgText = obj.toString();
                    mDB.updateMsg(msg);

                    CLog.d("newMsg content update complete.");
//                    DataKeyUtil.setDBKey(mContext, DB_NEW_MSG_CONTENT, obj.toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
