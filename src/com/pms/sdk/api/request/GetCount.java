package com.pms.sdk.api.request;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.db.PMSDB;

public class GetCount implements IPMSConsts
{

    private Context mContext;
    private APIManager apiManager;
    private Prefs mPrefs;
    private PMSDB mDB;

    public GetCount(Context context) {
        this.mContext = context;
        this.apiManager = APIManager.getInstance(context);
        this.mPrefs = apiManager.getPrefs();
        this.mDB = apiManager.getPmsDb();
    }

    public JSONObject getParam () {
        JSONObject jobj;

        try {
            jobj = new JSONObject();
            PMS pms = PMS.getInstance(mContext);
            jobj.put("reqUserMsgId", pms.getMaxUserMsgId());

            return jobj;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void request (final APICallback apiCallback) {
        try {
            apiManager.call(API_GET_COUNT, getParam(), new APICallback() {
                @Override
                public void response (String code, JSONObject json) {
                    if (CODE_SUCCESS.equals(code)) {
                        requiredResultProc(json);
                    }
                    if (apiCallback != null) {
                        apiCallback.response(code, json);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean requiredResultProc (JSONObject json) {
        try {
            DataKeyUtil.setDBKey(mContext, DB_NEW_MSG_COUNT, json.getString("newMsgCount"));
        } catch (JSONException e) {
            e.printStackTrace();
            DataKeyUtil.setDBKey(mContext, DB_NEW_MSG_COUNT, "0");
        }
        return true;
    }
}
