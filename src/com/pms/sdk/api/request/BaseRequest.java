package com.pms.sdk.api.request;

import android.content.Context;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.db.PMSDB;

public class BaseRequest implements IPMSConsts {

	protected Context mContext;
	protected APIManager apiManager;
	protected PMSDB mDB;
	protected Prefs mPrefs;

	protected boolean isComplete;

	public BaseRequest(Context context) {
		this.mContext = context;
//		this.apiManager = new APIManager(mContext);
		this.mPrefs = new Prefs(context);
		this.mDB = PMSDB.getInstance(context);
	}
}
