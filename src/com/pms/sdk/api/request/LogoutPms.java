package com.pms.sdk.api.request;

import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.db.PMSDB;

public class LogoutPms implements IPMSConsts
{
	private Context mContext;
	private APIManager apiManager;
	private Prefs mPrefs;
	private PMSDB mDB;

	public LogoutPms(Context context) {
		this.mContext = context;
		this.apiManager = APIManager.getInstance(context);
		this.mPrefs = apiManager.getPrefs();
		this.mDB = apiManager.getPmsDb();
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final APICallback apiCallback) {
		try {
			PMSUtil.setCustId(mContext, "");
			DataKeyUtil.setDBKey(mContext, DB_LOGINED_CUST_ID, "");
			DataKeyUtil.setDBKey(mContext, DB_MAX_USER_MSG_ID, "-1");
			mDB.deleteAll();

			apiManager.call(API_LOGOUT_PMS, new JSONObject(), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		return true;
	}
}
