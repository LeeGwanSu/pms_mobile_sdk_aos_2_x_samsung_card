package com.samsung_card.app.activity;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.pms.demo.R;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.PMSPopup;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.*;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSPopupUtil.btnEventListener;
import com.pms.sdk.common.util.PMSPopupUtil.touchEventListener;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

public class MainActivity extends Activity implements Serializable, IPMSConsts {

    private static final long serialVersionUID = 1L;

    private transient PMS pms = null;
    private PMSPopup pmsPopup = null;
    private final btnEventListener btnEvent = new btnEventListener() {

        private static final long serialVersionUID = 1L;

        @Override
        public void onClick() {
            pmsPopup.getActivity().finish();
        }
    };
    private final btnEventListener btnEvent1 = new btnEventListener() {

        private static final long serialVersionUID = 1L;

        @Override
        public void onClick() {
            pmsPopup.startNotiReceiver();
            pmsPopup.getActivity().finish();
        }
    };
    private final touchEventListener touchEvent = new touchEventListener() {

        private static final long serialVersionUID = 1L;

        @Override
        public void onTouch() {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.parse(pmsPopup.getWebLinkUrl());
            intent.setData(uri);
            pmsPopup.getActivity().startActivity(intent);
        }
    };
    private Context mContext = null;
    private EditText mEdtAppKey = null;
    private EditText mEdtMsgApiUrl = null;
    private EditText mEdtPrivateUrl = null;
    private EditText mEdtCustId = null;
    private Button mBtnDeviceCert = null;
    private Button mBtnLoginPms = null;
    private Button mBtnNewMsg = null;
    private Button mBtnReadMsg = null;
    private Button mBtnSetConfig = null;
    private Button mBtnLogoutPms = null;
    private TextView mTxtResult = null;
    private transient OnClickListener onClickListener = new OnClickListener() {

        /*
         * (non-Javadoc)
         *
         * @see android.view.View.OnClickListener#onClick(android.view.View)
         */
        @Override
        public void onClick(View v) {
            mTxtResult.setText("loading...");

            if (!StringUtil.isEmpty(String.valueOf(mEdtAppKey.getText()))) {
                PMSUtil.setApplicationKey(mContext, String.valueOf(mEdtAppKey.getText()));
                PMSUtil.setServerUrl(mContext, String.valueOf(mEdtMsgApiUrl.getText()));
                URI url = null;
                try {
                    url = new URI(String.valueOf(mEdtPrivateUrl.getText()));
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

                if (url.getScheme().equals("tcp")) {
                    PMSUtil.setMQTTServerUrl(mContext, "", String.valueOf(mEdtPrivateUrl.getText()));
                } else {
                    PMSUtil.setMQTTServerUrl(mContext, String.valueOf(mEdtPrivateUrl.getText()), "");
                }
                getServerSetting(true);
            }

            int id = v.getId();
            switch (id) {
                case R.id.btn_device_cert:
                    /**
                     * deviceCert (앱이 실행 되는 시점에서 호출 해주시면 됩니다.) 첫번째 파라미터는 CRM데이터 연동을 위한 파라미터로 null로 념겨주시면 됩니다.
                     */
                    new DeviceCert(mContext).request(null, new APICallback() {
                        @Override
                        public void response(String arg0, final JSONObject arg1) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run()
                                {
                                    mTxtResult.setText(arg1.toString());

                                }
                            });
                        }
                    });
                    break;

                case R.id.btn_login_pms:
                    /**
                     * 로그인(cust_id저장)을 수행합니다. 사용자 정보를 원치 않으시면 null 등록
                     */
                    new LoginPms(mContext).request(mEdtCustId.getText().toString(), null, new APICallback() {
                        @Override
                        public void response(String arg0, final JSONObject arg1) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run()
                                {
                                    mTxtResult.setText(arg1.toString());

                                }
                            });
                        }
                    });
                    break;

                case R.id.btn_new_msg:
                    // 현재 가지고 있는 메시지의 max user msg id를 가져올 수 있습니다.
                    String req = pms.getMaxUserMsgId();

                    /**
                     * 서버에서 메시지를 가져 와서 SQLite에 저장하는 request입니다. callback에서 pms.selectMsgList(1, 9999)과 같이 cursor를 가져와 쓰시거나, newMsg완료시점에 broadcasting을
                     * 하기 때문에, PMS.RECEIVER_REQUERY로 receiver를 받으셔서 메시지를 가져오셔도 됩니다. 파라미터 관련해서는 api문서를 참조하시면 되겠니다.
                     */
                    new NewMsg(mContext).request(PMS.TYPE_NEXT, req, "-1", "1", "30", new APICallback() {
                        @Override
                        public void response(String arg0, final JSONObject arg1) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run()
                                {
                                    // 메시지를 cursor형태로 가져올수 있습니다.
                                    Cursor c = pms.selectMsgList(1, 9999);
                                    mTxtResult.setText("msgListSize:" + c.getCount() + "\n\n" + arg1.toString());
                                    PMSDB pmsDB = PMSDB.getInstance(mContext);
                                    pmsDB.showAllTable(Msg.TABLE_NAME);

                                }
                            });
                        }
                    });
                    break;

                case R.id.btn_read_msg:
                    /**
                     * 메세지 읽을 처리 Class 입니다.
                     */
                    JSONArray jsAr = new JSONArray();
                    jsAr.put(PMSUtil.getReadParam("1"));

                    new ReadMsg(mContext).request(jsAr, new APICallback() {
                        @Override
                        public void response(String arg0, final JSONObject arg1) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run()
                                {
                                    mTxtResult.setText(arg1.toString());

                                }
                            });
                        }
                    });
                    break;

                case R.id.btn_set_config:
                    /**
                     * msg & noti flag 값을 저장하는 Class 입니다.
                     */
                    new SetConfig(mContext).request("Y", "Y", new APICallback() {
                        @Override
                        public void response(String arg0, final JSONObject arg1) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run()
                                {
                                    mTxtResult.setText(arg1.toString());

                                }
                            });
                        }
                    });
                    break;

                case R.id.btn_logout_pms:
                    /**
                     * Logout 을 실행하는 Class 입니다.
                     */
                    new LogoutPms(mContext).request(new APICallback() {
                        @Override
                        public void response(String arg0, final JSONObject arg1) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run()
                                {
                                    mTxtResult.setText(arg1.toString());

                                }
                            });
                        }
                    });
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;

        setPMSSetting();
        setPMSPopupSetting();

        mEdtAppKey = (EditText) findViewById(R.id.edt_app_key);
        mEdtMsgApiUrl = (EditText) findViewById(R.id.edt_msg_api);
        mEdtPrivateUrl = (EditText) findViewById(R.id.edt_private_url);
        mEdtCustId = (EditText) findViewById(R.id.edt_cust_id);

        mBtnDeviceCert = (Button) findViewById(R.id.btn_device_cert);
        mBtnLoginPms = (Button) findViewById(R.id.btn_login_pms);
        mBtnNewMsg = (Button) findViewById(R.id.btn_new_msg);
        mBtnReadMsg = (Button) findViewById(R.id.btn_read_msg);
        mBtnSetConfig = (Button) findViewById(R.id.btn_set_config);
        mBtnLogoutPms = (Button) findViewById(R.id.btn_logout_pms);

        mTxtResult = (TextView) findViewById(R.id.txt_result);

        mBtnDeviceCert.setOnClickListener(onClickListener);
        mBtnLoginPms.setOnClickListener(onClickListener);
        mBtnNewMsg.setOnClickListener(onClickListener);
        mBtnReadMsg.setOnClickListener(onClickListener);
        mBtnSetConfig.setOnClickListener(onClickListener);
        mBtnLogoutPms.setOnClickListener(onClickListener);

        PMSUtil.setApplicationKey(mContext, "android2");

        mEdtAppKey.setText(PMSUtil.getApplicationKey(mContext));
        mEdtMsgApiUrl.setText(PMSUtil.getServerUrl(mContext));
        mEdtPrivateUrl.setText(PMSUtil.getMQTTServerUrl(mContext));
        mEdtCustId.setText(pms.getCustId());

//        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_PHONE_STATE},0);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification =  new NotificationCompat.Builder(getApplicationContext(), "0")
                .setContentTitle("Title")
                .setContentText("Text")
                .setSmallIcon(R.drawable.ic_launcher)
                .build();
        notificationManager.notify(124124, notification);

        FirebaseInstanceId.getInstance().getInstanceId()
                          .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                              @Override
                              public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                  if (!task.isSuccessful()) {
                                      CLog.w("getInstanceId failed"+ task.getException());
                                      return;
                                  }

                                  // Get new Instance ID token
                                  String token = task.getResult().getToken();

                                  CLog.d("addOnCompleteListener "+token);
                              }
                          });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PMS.clear();
    }

    private void setPMSSetting() {
        // pms 기본 셋팅입니다.
        pms = PMS.getInstance(mContext);
        pms.setPopupSetting(false, "DreamC");
        pms.setPopupNoti(false);
        pms.setRingMode(true);
        pms.setVibeMode(true);
        pms.setIsPopupActivity(false);
        pms.setNotiOrPopup(true);
        pms.setNotificationGroupable(true);
        pms.setNotificationStackable(true);
        pms.setNotificationBadgeEnable(false);
    }

    @SuppressWarnings("static-access")
    private void setPMSPopupSetting() {
        // pms popup 기본 셋팅입니다.
        pmsPopup = pms.getPopUpInstance();
        pmsPopup.setXmlAndDefaultFlag(true);

        setXML();

        pmsPopup.commit();
    }

    private void setXML() {
        // xml 파일로 팝업창을 생성시
        pmsPopup.setLayoutXMLTextResId("pms_text_popup");
        pmsPopup.setXMLTextButtonType("TextView", "TextView");
        pmsPopup.setXMLTextButtonTagName("button1", "button2");

        pmsPopup.setLayoutXMLRichResId("pms_rich_popup");
        pmsPopup.setXMLRichButtonType("ImageView");
        pmsPopup.setXMLRichButtonTagName("button1");

        pmsPopup.setTextBottomBtnClickListener(btnEvent, btnEvent1);
        pmsPopup.setRichBottomBtnClickListener(btnEvent);
        pmsPopup.setRichLinkTouchListener(touchEvent);
    }

    private void getServerSetting(Boolean state) {
        if (state) {
            DataKeyUtil.setDBKey(mContext, DB_APP_KEY_CHECK, FLAG_Y);
            DataKeyUtil.setDBKey(mContext, DB_API_SERVER_CHECK, FLAG_Y);
            DataKeyUtil.setDBKey(mContext, DB_MQTT_SERVER_CHECK, FLAG_Y);
        } else {
            DataKeyUtil.setDBKey(mContext, DB_APP_KEY_CHECK, FLAG_N);
            DataKeyUtil.setDBKey(mContext, DB_API_SERVER_CHECK, FLAG_N);
            DataKeyUtil.setDBKey(mContext, DB_MQTT_SERVER_CHECK, FLAG_N);
        }
    }
}
